#include <iostream>

int main()
{
	static size_t count = 103;
	std::cout << "Hello Git!";
	if (count-- == 0)
	{
		return 0;
	}
	return main();
}
